<?php

    if(isset($_SERVER['QUERY_STRING'])) {
        // this is a new key-based (lookup) permalink;

        $key = $_SERVER['QUERY_STRING'];

        $MyApplicationId = "";
        $MyParseRestAPIKey = "";

        $headers = array(
        "X-Parse-Application-Id: $MyApplicationId",
        "X-Parse-REST-API-Key: $MyParseRestAPIKey"
        );

        $curlSession = curl_init();
        $request = 'where={"objectId":"' . $key . '"}';
        $url = "https://api.parse.com/1/classes/permaLinks" . "?" . $request;
        curl_setopt($curlSession, CURLOPT_URL, $url);
        curl_setopt($curlSession, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curlSession, CURLOPT_BINARYTRANSFER, true);
        curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);
        $data = curl_exec($curlSession);
        curl_close($curlSession);
        $json_array = json_decode($data, true);
        // print_r($json_array);
        $path = $json_array["results"]["0"]["path"];
        if ($path != "") {
            $destination = "http://onesearch.library.wwu.edu/primo_library/libweb/action/dlSearch.do?" . $path;
           header ('HTTP/1.1 301 Moved Permanently');
           header ('Location: ' . $destination );
        } else {
            $destination = "http://onesearch.library.wwu.edu/";
            echo "An error occurred; we could not find that permalink. <br> Please continue to <a href='" . $destination . "'>" . $destination . "</a>";
            # TODO: notify admin of this error;
        }

        exit();
    }

?>
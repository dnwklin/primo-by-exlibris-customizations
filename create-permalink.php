<?php


if (($domain == 'onesearch.library.wwu.edu') || ($domain == 'search.library.wwu.edu') || ($domain == 'alliance-primo.hosted.exlibrisgroup.com')) {
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST');
	header('Access-Control-Allow-Headers: EXLRequestType, Origin, Content-Type, Accept');
    header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');
}

    if(isset($_SERVER['QUERY_STRING'])) {
        // this is a new key-based (lookup) permalink;

        $MyApplicationId = "from-parse-dot-com";
        $MyParseRestAPIKey = "see-your-parse-account";

        $headers = array(
			"X-Parse-Application-Id: $MyApplicationId"
			, "X-Parse-REST-API-Key: $MyParseRestAPIKey"
			, "Content-Type: application/json"
        );

        $curlSession = curl_init();
		$querystring = $_SERVER['QUERY_STRING'];
		$querystring_parts = explode("&callback=jQuery", $querystring);
		$thePath = $querystring_parts[0];			// we don't want to store the jqueryCallback info
        $data = '{"path":"' . $thePath . '"}';

        $url = "https://api.parse.com/1/classes/permaLinks";
        curl_setopt($curlSession, CURLOPT_URL, $url);
        curl_setopt($curlSession, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curlSession, CURLOPT_BINARYTRANSFER, true);
        curl_setopt($curlSession, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curlSession,CURLOPT_POST,1);
		curl_setopt($curlSession, CURLOPT_POSTFIELDS, $data);
        $response = curl_exec($curlSession);
        curl_close($curlSession);

		echo $_GET['callback']."(".  $response .");";

        exit();
    }


?>
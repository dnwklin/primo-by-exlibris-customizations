# README #

This repository includes files used to customize the interface of Primo (by Ex Libris) at onesearch.library.wwu.edu.  

Here is our primary test view:
http://onesearch.library.wwu.edu/primo_library/libweb/action/search.do?vid=WWU_TEST

![screenshot](https://dl.dropboxusercontent.com/s/kn5uuty7h78vj2b/2014-12-16%2012_23_24-OneSearch%20-%20gluten.png?dl=0)

Please note that these customizations require that you use the tabbed scope interface in the PBO; it will not work with the standard select-list scope list.

To use this in your environment, we recommend that you create a test view (using the Primo Back Office, or PBO), and load (or point to) these files.  I prefer to have our header, footer and css files hosted on our website, instead of uploading them to the PBO.  To use the Entypo font/icons, I uploaded the Entypo files to our webserver, and had to add the .htaccess file to that folder.

### Features ###
1. fixed header with new menu layout
2. scope tabs
3. "were you looking for ____"?
4. permaLinks
5. jump to page
6. new icons
7. checkbox facets
8. 'more options' now expands facet lists without popup

![screenshot](https://dl.dropboxusercontent.com/s/1jzsamo267px3cr/2014-11-17%2012_24_19-OneSearch%20-%20whatcom.png?dl=0)

### How do I get set up? ###

* Summary of set up
	 * https://bitbucket.org/davidbasswwu/primo-by-exlibris-customizations/wiki/Scopes%20and%20tabs%20setup%20in%20the%20Primo%20Back%20Office
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Who do I talk to? ###

* david.bass at wwu dot edu